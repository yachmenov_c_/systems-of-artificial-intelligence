#!/usr/bin/php
<?php
function get_input() {
    return trim( fgets( STDIN ) );
}

$ph = [0.6, 0.3, 0.1];
$m = count($ph);
$pplus = [0.8, 0.5, 0.3];
$pminus = [0.4, 0.6, 0.7];

print("У Вас є температура? Так - 1; Ні - (-1); Незнаю - 0.\nВевдіть відповідну цифру.\n");
$w = get_input();

print("Ph[i]     |\tPh[i]/S\n");
print("----------------------------\n");
if ($w == 1) {
    for ($i = 0; $i < $m; $i++) {
        $phis[$i] = ($pplus[$i] * $ph[$i]) / ($pplus[$i] * $ph[$i] + $pminus[$i] * (1 - $ph[$i]));
        print($ph[$i] . "   |\t" . $phis[$i] . "\n");
    }
} elseif ($w == -1) {
    for ($i = 0; $i < $m; $i++) {
        $phis[$i] = ((1 - $pplus[$i]) * $ph[$i]) / (1 - $pplus[$i] * $ph[$i] - $pminus[$i] * (1 - $ph[$i]));
        print($ph[$i] . "   |\t" . $phis[$i] . "\n");
    }
} elseif ($w == 0) {
    for ($i = 0; $i < $m; $i++) {
        $phis[$i] = $ph[$i];
        print($ph[$i] . "   |\t" . $phis[$i] . "\n");
    }
} else {
    print("Ви ввели невідповідну цифру! Перезапустіть програму!\n");
}
