#!/usr/bin/php
<?php
function get_input() {
    return trim( fgets( STDIN ) );
}

$ph = 0.6;
$pmax = $ph;
$pplus = [0.8, 0.5, 0.3];
$m = count($pplus);
$pminus = [0.4, 0.6, 0.8];

for ($i = 0; $i < $m; $i++) {
    if ($pplus[$i] > $pminus[$i]) {
        $pmax = ($pplus[$i] * $pmax) / ($pplus[$i] * $pmax + $pminus[$i] * (1 - $pmax));
    } elseif ($pplus[$i] < $pminus[$i]) {
        $pmax = ((1 - $pplus[$i]) * $pmax) / (1 - $pplus[$i] * $pmax - $pminus[$i] * (1 - $pmax));
    }
}
print("У вас є температура? Так - 1; Нет - (-1); Незнаю - 0.\nВведіть відповідну цифру.\n");
$w = get_input();
if ($w == 1) {
    $ph = ($pplus[0] * $ph) / ($pplus[0] * $ph + $pminus[0] * (1 - $ph));
} elseif ($w == -1) {
    $ph = ((1 - $pplus[0]) * $ph) / (1 - $pplus[0] * $ph - $pminus[0] * (1 - $ph));
} elseif ($w != 0) {
    print("Ви ввели невідповідну цифру!\n");
}
/////////////////////////////////////////
print("У вас є нежить? Так - 1; Нет - (-1); Незнаю - 0.\nВведіть відповідну цифру.\n");
$w = get_input();
if ($w == 1) {
    $ph = ($pplus[1] * $ph) / ($pplus[1] * $ph + $pminus[1] * (1 - $ph));
} elseif ($w == -1) {
    $ph = ((1 - $pplus[1]) * $ph) / (1 - $pplus[1] * $ph - $pminus[1] * (1 - $ph));
} elseif ($w != 0) {
    print("Ви ввели невідповідну цифру!\n");
}
///////////////////////////////////////
print("У вас є головний біль? Так - 1; Нет - (-1); Незнаю - 0.\nВведіть відповідну цифру.\n");
$w = get_input();
if ($w == 1) {
    $ph = ($pplus[2] * $ph) / ($pplus[2] * $ph + $pminus[2] * (1 - $ph));
} elseif ($w == -1) {
    $ph = ((1 - $pplus[2]) * $ph) / (1 - $pplus[2] * $ph - $pminus[2] * (1 - $ph));
} elseif ($w != 0) {
    print("Ви ввели невідповідну цифру!\n");
}
//////////////////////////////////////////////////////////////////
printf("Pmax = %.5f", $pmax);
printf("Нижня границя = %.5f", 0.2 * $pmax);
printf("Верхня границя = %.5f", 0.2 * $pmax);
printf("Ph = %.5f", $ph);
if ($ph < 0 && $ph > $pmax) {
    print("Щось пішло не так! Спробуйте знову!\n");
} elseif ($ph >= 0 && $ph <= (0.2 * $pmax)) {
    print("Теорія про грип не сравджується! Ви не хворі!\n");
} elseif ($ph > (0.2 * $pmax) && $ph < (0.8 * $pmax)) {
    print("Теория про грип залишається! Рішення не знайдено!\n");
} elseif ($ph >= (0.8 * $pmax) && $ph <= $pmax) {
    print("Теория про грип підтверджується! Ви хворі!");
}