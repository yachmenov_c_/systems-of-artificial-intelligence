const training_set_input = [[["1","1","1"],["1","0","1"],["1","0","1"],["1","0","1"],["1","1","1"]],[["1","0","0"],["1","0","0"],["1","0","0"],["1","0","0"],["1","0","0"]],[["0","1","0"],["0","1","0"],["0","1","0"],["0","1","0"],["0","1","0"]],[["0","0","1"],["0","0","1"],["0","0","1"],["0","0","1"],["0","0","1"]],[["1","1","1"],["0","0","1"],["1","1","1"],["1","0","0"],["1","1","1"]],[["1","1","1"],["0","0","1"],["1","1","1"],["0","0","1"],["1","1","1"]],[["1","0","1"],["1","0","1"],["1","1","1"],["0","0","1"],["0","0","1"]],[["1","1","1"],["1","0","0"],["1","1","1"],["0","0","1"],["1","1","1"]],[["1","1","1"],["1","0","0"],["1","1","1"],["1","0","1"],["1","1","1"]],[["1","1","1"],["1","0","1"],["0","0","1"],["0","0","1"],["0","0","1"]],[["1","1","1"],["1","0","1"],["1","1","1"],["1","0","1"],["1","1","1"]],[["1","1","1"],["1","0","1"],["1","1","1"],["0","0","1"],["1","1","1"]]];
const training_set_output = [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1];

let output = [];
let synaptic_weights = [];
let number_of_training_iterations = 10000;

/* Random weights */
for (let i = 0; i < 15; i++) {
    synaptic_weights[i] = getRandomArbitrary(-1, 1);
}

/* Training the neuron */
do {
    let error = [], gradient = [], adjustment = [], training_set_input_2D = [];
    output = neuronThink(training_set_input, synaptic_weights);
    for (let i = 0; i < output.length; i++) {
        error.push(training_set_output[i] - output[i]);
        gradient.push(sigmoid(output[i], true));
    }
    for (let i = 0; i < training_set_input.length; i++) {
        training_set_input_2D.push(oneDimension(training_set_input[i]));
    }
    let training_set_input_t = training_set_input_2D[0].map((col, i) => training_set_input_2D.map(row => row[i]));
    for (let i = 0; i < training_set_input_t.length; i++) {
        let sum = 0;
        for (let j = 0; j < training_set_input_t[i].length; j++) {
            sum += training_set_input_t[i][j] * error[j] * gradient[j];
        }
        adjustment.push(sum);
    }
    for (let i = 0; i < synaptic_weights.length; i++) {
        synaptic_weights[i] += adjustment[i];
    }
    number_of_training_iterations--;
} while (number_of_training_iterations);

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function oneDimension(set) {
    let output = [];
    for (let i = 0; i < set.length; i++) {
        for (let j = 0; j < set[i].length; j++) {
            output.push(set[i][j]);
        }
    }
    return output;
}

function sigmoid(x, derivative) {
    let fx = 1 / (1 + Math.exp(-x));
    /* How convenient we are about the existing weight. This is a gradient. */
    if (derivative)
        return fx * (1 - fx);
    return fx;
}

function neuronThink(input, weights) {
    let sum_set = [];
    for (let i = 0; i < input.length; i++) {
        let sum = 0;
        let input_one_dimension = oneDimension(input[i]);
        for (let j = 0; j < input_one_dimension.length; j++) {
            sum += input_one_dimension[j] * weights[j];
        }
        sum_set.push(sum);
    }
    for (let i = 0; i < sum_set.length; i++) {
        sum_set[i] = sigmoid(sum_set[i]);
    }
    return sum_set;
}

function calculateNumber() {
    let values = [];
    for (let i = 0; i < 5; i++) {
        values[i] = [];
        for (let j = 0; j < 3; j++) {
            values[i][j] =
                document.querySelector('.compute > label > input[type="number"][name="n[' + i + '][' + j + ']"').value;
        }
    }
    let result = Math.round(neuronThink([values], synaptic_weights)[0]);
    let result_block = document.querySelector('.result > span');
    result_block.innerText = result ? 'Непарне число' : 'Парне число';
    result_block.style.color = result ? 'red' : 'green';
}

/* Events */
document.querySelectorAll('.compute > label > input[type="number"]').forEach(function(element){
    let colorChange = function(target){
        if ('1' === target.value) {
            target.style.backgroundColor = 'black';
        } else {
            target.style.backgroundColor = 'white';
        }
    };
    let mainEvent = function(e){
        let target = e.target;
        colorChange(target);
        calculateNumber();
    };
    element.addEventListener('change', mainEvent);
    element.addEventListener('click', function(e) {
        e.target.value = '1' === e.target.value ? '0' : '1';
        mainEvent(e);
    });
    colorChange(element);
});
/* Run */
calculateNumber();